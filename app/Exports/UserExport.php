<?php

namespace App\Exports;
use App\Models\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class UserExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    function __construct($request) {
        $this->request = $request;
    }

    public function collection()
    {
        $user_data =[];
        if($this->request->from_date && $this->request->to_date){
            $from_date = usDate($this->request->from_date);
            $to_date = usDate($this->request->to_date);
            $users = User::whereBetween('created_at',[$from_date,$to_date])->get();

            foreach($users as $user){
                $user_data[] = [
                    'name' => $user->first_name.' '.$user->last_name,
                    'mobile' => $user->mobile,
                    'email' => $user->email,
                ];
            }
        }else{
            $users = User::all();

            foreach($users as $user){
                $user_data[] = [
                    'name' => $user->first_name.' '.$user->last_name,
                    'mobile' => $user->mobile,
                    'email' => $email,
                ];
            }
        }
        $collection = collect($user_data);
        return $collection;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Mobile',
            'Email',
        ];
    }
}
