<?php

namespace App\Http\Controllers\Api;
use App\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Product as ProductResource;

class ProductController extends Controller
{
    public function getProducts(Request $request){
        $rules = [
            'pageLength' => 'numeric',
            'orderBy' => 'in:ASC,DESC',
            'sortBy' => 'in:id,created_at,price,brand,type,color,ratings',
        ];
        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return response()->json(['errors' => $validator->messages()],400);
        }

        if($request->product_id){
            $products = Product::where('id',$request->product_id)->first();
        }else{
            $page_length = 10;
            if($request->pageLength != ''){
                $page_length = $request->pageLength;
            }

            $products = Product::where('id','!=',0)
            ->when($request->brand, function($query) use ($request){
                return $query->where('brand',$request->brand);
            })
            ->when($request->type, function($query) use ($request){
                return $query->where('type',$request->type);
            })
            ->when($request->color, function($query) use ($request){
                return $query->where('color',$request->color);
            })
            ->when($request->ratings, function($query) use ($request){
                return $query->where('ratings',$request->ratings);
            })
            ->when(!$request->orderBy, function($query) use ($request){
                return $query->orderBy('id','ASC');
            })
            ->when($request->orderBy && !$request->sortBy, function($query) use ($request){
                return $query->orderBy('id',$request->orderBy);
            })
            ->when($request->sortBy, function($query) use ($request){
                if($request->orderBy != ''){
                    return $query->orderBy($request->sortBy,$request->orderBy);
                }
                if($request->orderBy == ''){
                    return $query->orderBy($request->sortBy,'ASC');
                }
            })
            ->when($request->search, function($query) use ($request){
                return $query->where('name','like','%'.$request->search.'%')->orwhere('type','like','%'.$request->search.'%')->orwhere('brand','like','%'.$request->search.'%');
            })
            ->paginate($page_length);
        }
        if($products){
            return response()->json(new ProductResource($products))->setStatusCode(200);
        }else{
            return response()->json(['message' => 'products not found'],400);
        }
    }
}
