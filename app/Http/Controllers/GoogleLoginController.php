<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Socialite;
use Auth;
use Exception;
use App\Models\User;
class GoogleLoginController extends Controller
{
    public function googleRedirect(){
         return Socialite::driver('google')->redirect();
    }

    public function googleCallback(){
        try{
            $socialite_user = Socialite::driver('google')->user();
            $user = User::where('google_id', $socialite_user->id)->first();
            if($user){
                Auth::login($user);
                return redirect('/home');
            }else{
                $user = User::create([
                    'name' => $socialite_user->name,
                    'email' => $socialite_user->email,
                    'google_id'=> $socialite_user->id,
                    'password' =>  \Hash::make('demo@123'),
                ]);
                Auth::login($user);
                return redirect('/home');
            }
        } catch (Exception $e) {
            return \Redirect::back()->withErrors(['error' => 'something went wrong,please try again later']);
        }
    }

    public function home(){
        $user = \Auth::user();
        return view('home',compact('user'));
    }
}
