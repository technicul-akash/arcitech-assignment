<?php
use \App\Models\Json;

function ends_with($s,$t)
{
    return \Illuminate\Support\Str::endsWith($s,$t);
}

function str_singular($s)
{
    return \Illuminate\Support\Str::singular($s);
}

function snake_case($s)
{
    return \Illuminate\Support\Str::snake($s);
}

function str_plural($s)
{
    return \Illuminate\Support\Str::plural($s);
}

function usDate($date){
    $date = explode(' ',$date);
    $date = explode("/",$date[0],3);
    return $date[2]."-".$date[1]."-".$date[0];
}

function inDate($date){
    $date1 = explode(' ',$date);
    $date2 = explode('-',$date1[0]);
    return $date2[2]."/".$date2[1]."/".$date2[0];
}

function getMonths(){
    $months =[];
    for($i = 1; $i <= 12; $i++) {
        $months[] = date('F',mktime(0,0,0,$i,1,date('Y')));
    }
    return $months;
}

function getProducts(){
    $products = [
        [
            'brand' => 'APPLE',
            'name' =>'Apple iPhone 7',
            'price' => 50000,
            'type' => 'IOS',
            'color' => 'BLACK',
            'ratings' => 3,
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vero pariatur autem atque culpa eos fugit aliquam illo quidem quibusdam asperiores voluptatem nobis, sed temporibus, eum incidunt reiciendis molestias quaerat.
            ',
        ],
        [
            'brand' => 'APPLE',
            'name' =>'Apple iPhone 12',
            'price' => 80000,
            'type' => 'IOS',
            'color' => 'WHITE',
            'ratings' => 4,
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vero pariatur autem atque culpa eos fugit aliquam illo quidem quibusdam asperiores voluptatem nobis, sed temporibus, eum incidunt reiciendis molestias quaerat.
            ',
        ],
        [
            'brand' => 'APPLE',
            'name' =>'Apple iPhone 13',
            'price' => 100000,
            'type' => 'IOS',
            'color' => 'GREY',
            'ratings' => 5,
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vero pariatur autem atque culpa eos fugit aliquam illo quidem quibusdam asperiores voluptatem nobis, sed temporibus, eum incidunt reiciendis molestias quaerat.
            ',
        ],
        [
            'brand' => 'APPLE',
            'name' =>'Apple iPhone 13 Pro',
            'price' => 120000,
            'type' => 'IOS',
            'color' => 'BLACK',
            'ratings' => 5,
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vero pariatur autem atque culpa eos fugit aliquam illo quidem quibusdam asperiores voluptatem nobis, sed temporibus, eum incidunt reiciendis molestias quaerat.
            ',
        ],
        [
            'brand' => 'SAMSUNG',
            'name' =>'Samsung S7',
            'price' => 42000,
            'type' => 'ANDROID',
            'color' => 'BLACK',
            'ratings' => 5,
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vero pariatur autem atque culpa eos fugit aliquam illo quidem quibusdam asperiores voluptatem nobis, sed temporibus, eum incidunt reiciendis molestias quaerat.
            ',
        ],
        [
            'brand' => 'SAMSUNG',
            'name' =>'Samsung galexy M53',
            'price' => 25999,
            'type' => 'ANDROID',
            'color' => 'GREY',
            'ratings' => 2,
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vero pariatur autem atque culpa eos fugit aliquam illo quidem quibusdam asperiores voluptatem nobis, sed temporibus, eum incidunt reiciendis molestias quaerat.
            ',
        ],
        [
            'brand' => 'SAMSUNG',
            'name' =>'Samsung galexy A33',
            'price' => 30000,
            'type' => 'ANDROID',
            'color' => 'WHITE',
            'ratings' => 4,
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vero pariatur autem atque culpa eos fugit aliquam illo quidem quibusdam asperiores voluptatem nobis, sed temporibus, eum incidunt reiciendis molestias quaerat.
            ',
        ],
        [
            'brand' => 'XIAOMI',
            'name' =>'Xiaomi 11T ',
            'price' => 35000,
            'type' => 'ANDROID',
            'color' => 'BLUE',
            'ratings' => 5,
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vero pariatur autem atque culpa eos fugit aliquam illo quidem quibusdam asperiores voluptatem nobis, sed temporibus, eum incidunt reiciendis molestias quaerat.
            ',
        ],
        [
            'brand' => 'XIAOMI',
            'name' =>'Xiaomi 11 lite ',
            'price' => 10000,
            'type' => 'ANDROID',
            'color' => 'BLACK',
            'ratings' => 3,
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vero pariatur autem atque culpa eos fugit aliquam illo quidem quibusdam asperiores voluptatem nobis, sed temporibus, eum incidunt reiciendis molestias quaerat.
            ',
        ],
        [
            'brand' => 'XIAOMI',
            'name' =>'Redmi 10 prime',
            'price' => 12500,
            'type' => 'ANDROID',
            'color' => 'WHITE',
            'ratings' => 4,
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam vero pariatur autem atque culpa eos fugit aliquam illo quidem quibusdam asperiores voluptatem nobis, sed temporibus, eum incidunt reiciendis molestias quaerat.
            ',
        ],
    ];
    return $products;
   
}
?>