<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Technicul\Role;
use App\Models\Technicul\Permissiongroup;
use App\Models\Technicul\Permission;
use App\Models\User;

class PermissionSeeder extends Seeder
{
    private $permissions = [
        'PermissionGroup' => [ 
            'contoller' => 'Admin\Technicul\PermissionGroupController',
            'permissions' => [
                'permissiongroup-view' => [
                    'index',
                    'data',
                ],
            ]
        ],
        'Permission' => [ 
            'contoller' => 'Admin\Technicul\PermissionController',
            'permissions' => [
                'permission-view' => [
                    'index',
                    'data',
                    'getGroupPermissions',
                ],
            ]
        ],
        'Role' => [ 
            'contoller' => 'Admin\Technicul\RoleController',
            'permissions' => [
                'role-view' => [
                    'index',
                    'data',
                ],
                'role-store' => [
                    'create',
                    'store',
                ],
                'role-update' => [
                    'edit',
                    'update',
                ],
                'role-delete' => [
                    'delete',
                ],
                'role-permission' => [
                    'permissionsCreate',
                    'permissionsStore',
                ],
            ]
        ],
        'Dashboard' => [ 
            'contoller' => 'Admin\DashboardController',
            'permissions' => [
                'dashboard-view' => [
                    'index',
                    'data',
                    'getChartData',
                ],
            ]
        ],
        'User' => [ 
            'contoller' => 'Admin\UserController',
            'permissions' => [
                'user-view' => [
                    'index',
                    'data',
                ],
                'user-store' => [
                    'create',
                    'store',
                    'sampleExport',
                    'importUserData',
                ],
                'user-update' => [
                    'edit',
                    'update',
                    'changeStatus',
                ],
            ]
        ],
        'Report' => [ 
            'contoller' => 'Admin\ReportController',
            'permissions' => [
                'report-view' => [
                    'index',
                    'reportExport',
                ],
            ]
        ],
    ];
    
    
    private $roles = [
        'SuperAdmin' => [
            //Permission Groups
            'permissiongroup-view',
    
            //Permissions
            'permission-view',
    
            //Roles
            'role-view',
            'role-store',
            'role-update',
            'role-delete',
            'role-permission',

            //Dashboard
            'dashboard-view',

            //User
            'user-view',
            'user-store',
            'user-update',

            //Report
            'report-view',
        ],
        'Admin' => [
            //Roles
            'role-view',
            'role-store',
            'role-update',
            'role-delete',
            'role-permission',

            //Dashboard
            'dashboard-view',
            
            //User
            'user-view',
            'user-store',
            'user-update',

             //Report
             'report-view',
            
        ],
    ];
    
    private $users = [
        [
            'first_name'  => 'Tushar',
            'last_name'  => 'Ugale',
            'roles'  => [
                'SuperAdmin',
            ],
            'mobile'  => '8767444344',
            'email'  => 'tushar@technicul.com',
            'password' => 'tushar@123'
        ],
        [
            'first_name'  => 'Akash',
            'last_name'  => 'More',
            'roles'  => [
                'Admin',
            ],
            'mobile'  => '8888888888',
            'email'  => 'akash.technicul@gmail.com',
            'password' => 'akash@123'
        ],
    ];

    public function run(){
        //Groups & Permission
        $this->deletePermissions();
        $this->createPermissions();

        //Create Roles
        $this->createRoles();
        
        //Create Users
        $this->createUsers();
    }

    private function deletePermissions(){
        $permissions = Permission::all();
        foreach($permissions as $permission){
            $delete_permission = true;
            foreach($this->permissions as $group => $data){
                foreach($data['permissions'] as $p_name => $methods){
                    if($p_name == $permission->name){
                        $delete_permission = false;
                    }
                }
            } 
            if($delete_permission){
                $permission->delete();
            }
        } 
        
        $permission_groups = Permissiongroup::all();
        foreach($permission_groups as $permission_group){
            $delete_permissiongroup = true;
            foreach($this->permissions as $group => $data){
                if($group == $permission_group->name){
                    $delete_permissiongroup = false;
                }
            } 
            if($delete_permissiongroup){
                $permission_group->delete();
            }
        } 
    }

    private function createPermissions(){
        foreach($this->permissions as $group => $data){
            $permissiongroup = Permissiongroup::where('name',$group)->first();
            if(!$permissiongroup){
                $permissiongroup = new Permissiongroup;
                $permissiongroup->name = $group;
                $permissiongroup->controller = $data['contoller'];
                $permissiongroup->save();
            }else{
                $permissiongroup->controller = $data['contoller'];
                $permissiongroup->save();
            }
    
            foreach($data['permissions'] as $permissions_name => $methods){
                $permission = Permission::where('permissiongroup_id',$permissiongroup->id)->where('name',$permissions_name)->first();
                if(!$permission){
                    $permission = new Permission;
                    $permission->permissiongroup_id = $permissiongroup->id;
                    $permission->name = $permissions_name;
                    $permission->methods = $methods;
                    $permission->guard_name = config('auth.defaults.guard');
                    $permission->save();
                }else{
                    $permission->methods = $methods;
                    $permission->save();
                }
            }
        }
    }

    private function createRoles(){
        foreach($this->roles as $role_name => $permissions){
            $role = Role::where('name',$role_name)->first();
            if(!$role){
                $role = new Role;
                $role->name = $role_name;
                $role->guard_name = config('auth.defaults.guard');
                $role->save();
            }
            
            $permission_ids = Permission::whereIn('name',$permissions)->pluck('id');
            $role->syncPermissions($permission_ids);
        }
    }
    
    private function createUsers(){
        foreach($this->users as $data){
            $user = User::where('email',$data['email'])->where('mobile',$data['mobile'])->first();
            if(!$user){
                $user = new User;
                $user->email = $data['email'];
                $user->mobile = $data['mobile'];
                $user->first_name = $data['first_name'];
                $user->last_name = $data['last_name'];
                $user->password = \Hash::make($data['password']);
                $user->save();
            }else{
                $user->first_name = $data['first_name'];
                $user->last_name = $data['last_name'];
                $user->password = \Hash::make($data['password']);
                $user->save();
            }
    
            //Assign Role & Sync Permission to User
            $user->assignRole($data['roles']);

            //Sync All Roles Permissions to User
            $all_permissions = collect();
            foreach($data['roles'] as $role_name){
                $role = Role::where('name',$role_name)->first();
                $permissions = $role->permissions()->get();
                foreach($permissions as $permission){
                    $all_permissions->push($permission);
                }
            }
            $user->syncPermissions($all_permissions);
        }
    }
    
}
