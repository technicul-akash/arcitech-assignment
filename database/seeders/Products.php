<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Product;
class Products extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(getProducts() as $product_data){
            $product = new Product;
            $product->brand = $product_data['brand'];
            $product->name = $product_data['name'];
            $product->price = $product_data['price'];
            $product->type = $product_data['type'];
            $product->color = $product_data['color'];
            $product->ratings = $product_data['ratings'];
            $product->description = $product_data['description'];
            $product->save();
        }
    }
}
