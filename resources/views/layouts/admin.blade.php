<!DOCTYPE html>
<html class="loading" lang="en">
<!-- BEGIN : Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Apex admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Apex admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel Ready</title>
    <link rel="shortcut icon" type="image/x-icon" href="/theme/app-assets/img/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="/theme/app-assets/img/ico/favicon-32.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900%7CMontserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/vendors/css/prism.min.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/vendors/css/switchery.min.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/vendors/css/chartist.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN APEX CSS-->
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/css/themes/layout-dark.css">
    <link rel="stylesheet" href="/theme/app-assets/css/plugins/switchery.css">
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/vendors/css/pickadate/pickadate.css">
    <!-- END APEX CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/css/pages/dashboard1.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/theme/assets/css/style.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" type="text/css" href="/theme/app-assets/vendors/css/select2.min.css">

    <!-- END: Custom CSS-->
    {{-- Main Js --}}
    <script src="/theme/app-assets/vendors/js/vendors.min.js"></script>
    <script src="/theme/app-assets/vendors/js/switchery.min.js"></script>

    {{--Yajara Datatable --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
    <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>

     {{-- Date picker js / time picker js --}}
     <script src="/theme/app-assets/vendors/js/pickadate/picker.js"></script>
     <script src="/theme/app-assets/vendors/js/pickadate/picker.date.js"></script>
     <script src="/theme/app-assets/vendors/js/pickadate/picker.time.js"></script>
     <script src="/theme/app-assets/vendors/js/pickadate/picker.time.js"></script>

     {{-- Charts --}}
    <script src="/theme/app-assets/vendors/js/chart.min.js"></script>
    <script src="/theme/app-assets/js/charts-chartjs.js"></script>

    <!-- Date Range Picker -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    
</head>
<!-- END : Head-->

<!-- BEGIN : Body-->

<body class="vertical-layout vertical-menu 2-columns  navbar-sticky" data-menu="vertical-menu" data-col="2-columns">

    <nav class="navbar navbar-expand-lg navbar-light header-navbar navbar-fixed">
        <div class="container-fluid navbar-wrapper">
            <div class="navbar-header d-flex">
                <div class="navbar-toggle menu-toggle d-xl-none d-block float-left align-items-center justify-content-center" data-toggle="collapse"><i class="ft-menu font-medium-3"></i></div>
                <ul class="navbar-nav">
                    <li class="nav-item mr-2 d-none d-lg-block"><a class="nav-link apptogglefullscreen" id="navbar-fullscreen" href="javascript:;"><i class="ft-maximize font-medium-3"></i></a></li>
                </ul>
            </div>
            <div class="navbar-container">
                <div class="collapse navbar-collapse d-block" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="dropdown nav-item mr-1"><a class="nav-link dropdown-toggle user-dropdown d-flex align-items-end" id="dropdownBasic2" href="javascript:;" data-toggle="dropdown">
                                <div class="user d-md-flex d-none mr-2"><span class="text-right">{{\Auth::user()->first_name}} {{\Auth::user()->last_name}}</span></div>
                            </a>
                            <div class="dropdown-menu text-left dropdown-menu-right m-0 pb-0" aria-labelledby="dropdownBasic2">
                                <a class="dropdown-item" href="#">
                                    <div class="d-flex align-items-center"><i class="ft-edit mr-2"></i><span>Edit Profile</span></div>
                                </a>
                                <a class="dropdown-item" href="#" onclick="$('#logout-form').submit();">
                                    <div class="d-flex align-items-center"><i class="ft-power mr-2"></i><span>Logout</span></div>
                                </a>  
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                <!-- <div class="d-flex align-items-center">
                                    <form action="{{ route('logout') }}" method="post">
                                        @csrf
                                        <button id="logout" class="btn text-center" type="submit" style="margin-left:30%;"><span>Logout</span></button>
                                    </form>
                                </div> -->
                              
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- Navbar (Header) Ends-->

    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="wrapper">
        <!-- main menu-->
        <!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
        <div class="app-sidebar menu-fixed" data-background-color="man-of-steel" data-image="/theme/app-assets/img/sidebar-bg/01.jpg" data-scroll-to-active="true">
            <!-- main menu header-->
            <!-- Sidebar Header starts-->
            <div class="sidebar-header text-center">
                <div class="logo clearfix"><a class="logo-text text-center" href="/admin/dashboard" style="text-transform:capitalize;">
                        <span class="text">LARAVEL</span>
                    </a><a class="nav-toggle d-none d-lg-none d-xl-block" id="sidebarToggle" href="javascript:;"><i class="toggle-icon ft-toggle-right" data-toggle="expanded"></i></a><a class="nav-close d-block d-lg-block d-xl-none" id="sidebarClose" href="javascript:;"><i class="ft-x"></i></a></div>
            </div>
            <!-- Sidebar Header Ends-->
            <!-- / main menu header-->
            <!-- main menu content-->
            <div class="sidebar-content main-menu-content">
                
                <hr style="margin-top: 0.5rem; margin-bottom: 0.5rem">
                    <h6 class="text-center text-white p-1">ADMIN</h6>
                <hr  style="margin-top: 0.5rem; margin-bottom: 0.5rem">
                
                <div class="nav-container">
                    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                        @can('dashboard-view')
                        <li class="nav-item @if(Route::is('admin.dashboard.*')) active @endif" ><a href="/admin/dashboard"><i class="fa fa-tachometer"></i><span class="menu-title" data-i18n="Email">Dashboard</span></a>
                        </li>
                        @endcan
                        
                        @can('permissiongroup-view')
                        <li class="nav-item @if(Route::is('admin.technicul.permissiongroups.*')) active @endif" ><a href="/admin/technicul/permissiongroups"><i class="fas fa-layer-group"></i><span class="menu-title" data-i18n="Email">Permission Groups</span></a>
                        </li>
                        @endcan

                        @can('permission-view')
                        <li class="nav-item @if(Route::is('admin.technicul.permissions.*')) active @endif" ><a href="/admin/technicul/permissions"><i class="fas fa-layer-group"></i><span class="menu-title" data-i18n="Email">Permissions</span></a>
                        </li>
                        @endcan

                        @can('role-view')
                        <li class="nav-item @if(Route::is('admin.technicul.roles.*')) active @endif" ><a href="/admin/technicul/roles"><i class="fa fa-user-lock"></i><span class="menu-title" data-i18n="Email">Roles</span></a>
                        </li>
                        @endcan

                        @can('user-view')
                        <li class="nav-item @if(Route::is('admin.users.*')) active @endif" ><a href="/admin/users"><i class="fa fa-users"></i><span class="menu-title" data-i18n="Email">Users</span></a>
                        </li>
                        @endcan

                        @can('report-view')
                        <li class="has-sub nav-item"><a href="#"><i class="fas fa-file-excel"></i><span class="menu-title" data-i18n="Tables">Reports</span></a>
                            <ul class="menu-content">
                                <li class=" nav-item @if(last(request()->segments()) == 'users' && !Route::is('admin.users.*')  && !Route::is('admin.imports.*')) active @endif">
                                    <a href="/admin/reports/users"><i class="fas fa-user"></i><span class="menu-title" data-i18n="reports">Users</span></a>
                                </li>
                            </ul>
                        </li>
                        @endcan
                    </ul>
                </div>
            </div>
            <!-- main menu content-->
            <div class="sidebar-background"></div>
            <!-- main menu footer-->
            <!-- include includes/menu-footer-->
            <!-- main menu footer-->
            <!-- / main menu-->
        </div>

        <div class="main-panel">
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    @yield('content')
                </div>
            </div>
            <!-- END : End Main Content-->

            <!-- BEGIN : Footer-->
            <footer class="footer undefined undefined">
                <p class="clearfix text-muted m-0"><span>Copyright &copy; 2020 &nbsp;</span><a href="https://technicul.com/" id="pixinventLink" target="_blank">Technicul</a><span class="d-none d-sm-inline-block">, All rights reserved.</span></p>
            </footer>
            <!-- End : Footer-->
            <!-- Scroll to top button -->
            <button class="btn btn-primary scroll-top" type="button"><i class="ft-arrow-up"></i></button>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <!-- BEGIN VENDOR JS-->
  
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->

    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN APEX JS-->
    <script src="/theme/app-assets/js/core/app-menu.js"></script>
    <script src="/theme/app-assets/js/core/app.js"></script>
    <script src="/theme/app-assets/js/notification-sidebar.js"></script>
    <script src="/theme/app-assets/js/customizer.js"></script>
    <script src="/theme/app-assets/js/scroll-top.js"></script>
    <script src="/theme/app-assets/vendors/js/select2.full.min.js"></script>
    <script src="/theme/app-assets/js/components-modal.min.js"></script>
    <!-- END APEX JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    {{-- <script src="/theme/app-assets/js/dashboard1.js"></script> --}}
    <!-- END PAGE LEVEL JS-->
    <!-- BEGIN: Custom CSS-->
    <script src="/theme/assets/js/scripts.js"></script>
    <!-- END: Custom CSS-->
    <script>
        $(".select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });

        $('.pickadate').pickadate({
            format: 'dd/mm/yyyy',        
        });

        $('.pickadate-selectors').pickadate({
            format: 'dd/mm/yyyy',
            selectMonths: true,
            selectYears: 2,
            max: true,
            
        });
  
        $('.daterange').daterangepicker({ 
            startDate: moment().startOf('month'), 
            endDate: moment().endOf('month'),
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    </script>
</body>
<!-- END : Body-->

</html>