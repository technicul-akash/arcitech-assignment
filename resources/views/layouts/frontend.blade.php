<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Bilbao HTML Template</title>

<!-- Stylesheets -->
<link href="/frontend/css/bootstrap.css" rel="stylesheet">
<link href="/frontend/css/style.css" rel="stylesheet">
<link href="/frontend/css/responsive.css" rel="stylesheet">

<!-- Fav Icons -->
<link rel="shortcut icon" href="/frontend/images/favicon.png" type="image/x-icon">
<link rel="icon" href="/frontend/images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="/frontend/js/respond.js"></script><![endif]-->
</head>

<body>
<div class="page-wrapper">
    
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- main header -->
    <header class="main-header">

        <!-- header top -->
        <div class="header-top">
            <div class="container">
                <div class="outer-box clearfix">
                    <!--Top Left-->
                    <div class="top-left float-sm-left">
                        <div class="social-links clearfix">
                            <a href="/frontend/"><span class="fab fa-facebook-f"></span></a>
                            <a href="/frontend/"><span class="fab fa-twitter"></span></a>
                            <a href="/frontend/"><span class="fab fa-linkedin-in"></span></a>
                            <a href="/frontend/"><span class="fab fa-pinterest"></span></a>
                            <a href="/frontend/"><span class="fab fa-vimeo-v"></span></a>
                        </div>
                    </div>

                    <!--Top Right-->
                    <div class="top-right float-sm-right">
                        <ul class="topbar-info">
                            <li><a href="/frontend/">FAQ'S</a></li>
                            <li><a href="/frontend/">Support</a></li>
                            <li><a href="/frontend/">English</a></li>
                        </ul>   
                    </div>
                </div>
                    
            </div>
        </div>

        <!--Header-info-->
        <div class="header-info">
            <div class="container">
                <div class="clearfix">
                    <div class="float-left">
                        <div class="main-logo">
                            <a class="navbar-brand pr-50" href="index-2.html"><img src="/frontend/images/logo.png" class="logo" alt=""></a>
                        </div>
                    </div>
                    <div class="float-right">
                        <div class="header-contact-info">
                            <ul>
                                <li>
                                    <div class="iocn-holder">
                                        <span class="flaticon-time"></span>
                                    </div>
                                    <div class="text-holder">
                                        <h6>WORKING HOURS</h6>
                                        <p>Moday - Sunday: 8.00am to 10.30pm</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="iocn-holder">
                                        <span class="flaticon-phone-call"></span>
                                    </div>
                                    <div class="text-holder">
                                        <h6>CALL US</h6>
                                        <p>+91 9320102045</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="iocn-holder">
                                        <span class="flaticon-mail-1"></span>
                                    </div>
                                    <div class="text-holder">
                                        <h6>MAIL US</h6>
                                        <p>kavachsis@gmail.com</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>

        <!-- Header upper -->
        <div class="header-upper">
            <div class="container clearfix">
                    
                
                <div class="upper-right clearfix">
                    
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu navbar-expand-lg">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse justify-content-center align-items-center w-100">
                                <ul class="navigation clearfix">
                                    <li><a href="/">Home</a></li>
                                    <li><a href="about">About</a></li>
                                    <li><a href="services">Services</a></li>
                                    <li><a href="term-contract">Terms of Contracts</a></li>
                                    <li><a href="#">Our Clients</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    
                </div>
                    
            </div>
        </div>
        <!--End Header Upper-->

        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="container">
                <div class="clearfix">
                    <!--Logo-->
                    <div class="logo float-left">
                        <a href="index-2.html" class="img-responsive"><img src="images/logo.png" alt="" title=""></a>
                    </div>
                    
                    <!--Right Col-->
                    <div class="right-col float-right">
                        <!-- Main Menu -->
                        <nav class="main-menu navbar-expand-lg">
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li><a href="/">Home</a></li>
                                    <li><a href="about">About</a></li>
                                    <li><a href="services">Services</a></li>
                                    <li><a href="term-contract">Terms of Contracts</a></li>
                                    <li><a href="#">Our Clients</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </nav><!-- Main Menu End-->
                    </div>
                </div>
                    
                
            </div>
        </div>
        <!--End Sticky Header-->
    </header>

    <section>
        @yield('content')
    </section>

    <!-- Main footer -->
    <footer class="main-footer">
        <div class="container">
        
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">                            
                    <!--Footer Column-->
                    <div class="footer-column col-lg-4">
                        <div class="footer-widget about-widget">
                            <div class="footer-logo">
                                <figure>
                                    <a href="index-2.html"><img src="images/footer-logo.png" alt=""></a>
                                </figure>
                            </div>
                            <div class="widget-content">
                                <div class="text">Thusiastically mesh long-term high-impact infrastructures vis-a-vis service.Leverage agile frameworks to provide a robust synopsis for high level overviews. </div>
                                <ul class="contact-info">
                                    <li>Phone ; +987 654 3210</li>
                                    <li>Email ; <a href="/frontend/">security@support.com</a></li>
                                </ul>
                                <ul class="social-icon-three">
                                    <li><a href="/frontend/"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="/frontend/"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="/frontend/"><i class="fab fa-google-plus-g"></i></a></li>
                                    <li><a href="/frontend/"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="/frontend/"><i class="fab fa-vimeo-v"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End column -->                    
                    <!--Footer Column-->
                    <div class="footer-column col-lg-4">
                        <div class="footer-widget services-widget">
                            <h2 class="widget-title">All Services</h2>
                            <div class="widget-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul class="list">
                                            <li><a href="/frontend/">CCTV Cameras</a></li>
                                            <li><a href="/frontend/">Retail Security</a></li>
                                            <li><a href="/frontend/">Campus Safety</a></li>
                                            <li><a href="/frontend/">Marine Ship Security</a></li>
                                            <li><a href="/frontend/">Industrial Security</a></li>
                                            <li><a href="/frontend/">Government Facilities</a></li>
                                            <li><a href="/frontend/">Residential Security</a></li>
                                            <li><a href="/frontend/">Crossing Guards</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="list">
                                            <li><a href="/frontend/">Uniformed Officers</a></li>
                                            <li><a href="/frontend/">Mobile Patrol</a></li>
                                            <li><a href="/frontend/">Video Surveillance</a></li>
                                            <li><a href="/frontend/">Loss Prevention</a></li>
                                            <li><a href="/frontend/">Security Consulting</a></li>
                                        </ul>
                                    </div>
                                </div>
                                        
                            </div>
                        </div>  
                    </div>
                    <!-- End column -->
                    <!--Footer Column-->
                    <div class="footer-column col-lg-4">
                        <div class="footer-widget posts-widget">
                            <h2 class="widget-title">Recent Posts</h2>
                            <div class="widget-content">
                                <div class="posts">
                                    <div class="post">
                                        <figure class="post-thumb">
                                            <img src="images/resource/post-thumb-1.jpg" alt="">
                                            <a href="blog-single.html" class="overlay-link">
                                                <span class="fa fa-link"></span>
                                            </a>
                                        </figure>
                                        <div class="desc-text">
                                            <a href="blog-single.html">What Planning Process <br>Needs?</a>
                                        </div>
                                        <div class="time">06 June 2018</div>
                                    </div>

                                    <div class="post">
                                        <figure class="post-thumb">
                                            <img src="images/resource/post-thumb-2.jpg" alt="">
                                            <a href="blog-single.html" class="overlay-link">
                                                <span class="fa fa-link"></span>
                                            </a>
                                        </figure>
                                        <div class="desc-text">
                                            <a href="blog-single.html"> Tips To Move Your Project  More Forward.</a>
                                        </div>
                                        <div class="time">06 June 2018</div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <!-- End column -->
                </div>
            </div>
        </div>
        
        <!--Footer Bottom-->
         <div class="footer-bottom">
            <div class="container">
                <div class="clearfix">
                    <div class="float-sm-left">
                        <div class="copyright-text">
                            <p>Copyrights © 2018 <a href="index-2.html">Bilbao</a>. All rights reserved.</p>
                        </div>
                    </div>
                    <div class="float-sm-right">
                        <ul>
                            <li><a href="/frontend/">Privacy Policy</a></li>
                            <li><a href="/frontend/">erms & Condition</a></li>
                        </ul>
                    </div>
                </div>   
            </div>
        </div>
    </footer>


</div>
<!--End pagewrapper--> 

<!-- Scroll Top Button -->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-angle-up"></span>
</button>
    

<!-- jequery plugins -->

<script src="/frontend/js/jquery.js"></script>
<script src="/frontend/js/popover.js"></script>
<script src="/frontend/js/bootstrap.min.js"></script>

<script src="/frontend/js/wow.js"></script>
<script src="/frontend/js/owl.js"></script>
<script src="/frontend/js/validate.js"></script>
<script src="/frontend/js/mixitup.js"></script>
<script src="/frontend/js/isotope.js"></script>
<script src="/frontend/js/appear.js"></script>
<script src="/frontend/js/jquery.fancybox.js"></script>

<script src="/frontend/js/script.js"></script>

</body>

<!-- Mirrored from html.commonsupport.xyz/2018/Bilbao/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Oct 2022 07:39:55 GMT -->
</html>