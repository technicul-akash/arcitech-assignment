<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('google/redirect', 'GoogleLoginController@googleRedirect')->name('google.redirect');
Route::get('google/callback', 'GoogleLoginController@googleCallback')->name('google.callback');

Route::middleware(['auth'])->group(function () {
    Route::get('home', 'GoogleLoginController@home')->name('home'); 
});
require __DIR__.'/auth.php';
